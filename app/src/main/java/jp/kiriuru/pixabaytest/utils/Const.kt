package jp.kiriuru.pixabaytest.utils

class Const {
    companion object {
        const val BUNDLE = "items"
        const val API_KEY = "22189183-d9e96d94abe4ae7504f982890"
        const val BASE_URL = "https://pixabay.com/api/"
        const val TAG_MAIN = "MAIN"
        const val TAG_VM = "MAIN_VM"
    }
}