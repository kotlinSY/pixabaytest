Простое приложение по поиску и отображению картинок с Pixabay. Используется: Kotlin, MVVM, Coroutines, Glide, Retrofit, Fragment, Navigation Component, ViewBinding.

Скачать тест версию: [build 1.4](https://drive.google.com/file/d/1WsqslYLajMa9qQNb1nZrFPEE1zklSYlE/view?usp=sharing)
